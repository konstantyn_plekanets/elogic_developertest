/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

var config = {
    map: {
        '*': {
            weatherCheck: 'Elogic_DeveloperTest/js/weather/weather-check'
        }
    }
};
