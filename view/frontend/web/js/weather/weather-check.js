<!--
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */
-->
define([
    'jquery',
    'ko',
    'mage/url'
], function($, ko, urlBuilder) {
    "use strict";
    $.widget('mage.weatherCheck', {
        options: {
            url: 'weather/check/weather',
            weatherContent: '.weather-check',
            cityContent: '.weather-city',
            temperatureContent: '.weather-temperature',
            weatherSize: '.weather-size',
            time: '.weather-time',
            intervalValue: 600000
        },

        /**
         * @private
         */
        _create: function () {
            let self = this;
            self._init();

            let interval = window.setInterval(function() {
                    self.weatherCheck(self)
                },
                self.options.intervalValue
            )
        },

        /**
         * Initialize first weather data
         *
         * @private
         */
        _init: function () {
            this.weatherCheck(this);
        },

        /**
         * @param $widget
         */
        weatherCheck: function ($widget) {
            $.ajax({
                url: urlBuilder.build($widget.options.url),
                type: 'GET',
                contentType: 'application/json',
                success: function (res) {
                    if (res.city && res.temperature && res.time) {
                        let city = $($widget.options.weatherContent).find($widget.options.cityContent),
                            temperature = $($widget.options.weatherContent).find($widget.options.temperatureContent),
                            time = $($widget.options.weatherContent).find($widget.options.time),
                            weatherSize = $($widget.options.weatherContent).find($widget.options.weatherSize);

                        $(city).text(res.city);
                        $(temperature).text(res.temperature);
                        $(time).text(res.time);
                        $(weatherSize).show();
                    }
                },
                error: function (res) {
                    let weatherSize = $($widget.options.weatherContent).find($widget.options.weatherSize);
                    $(weatherSize).hide();
                }
            });
        }
    });

    return $.mage.weatherCheck;
});
