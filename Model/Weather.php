<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Model;

use Elogic\DeveloperTest\Api\Data\WeatherInterface;
use Magento\Framework\Model\AbstractModel;
use Elogic\DeveloperTest\Model\ResourceModel\ResourceModel;

/**
 * Class Weather
 * @package Elogic\DeveloperTest\Model
 */
class Weather extends AbstractModel implements  WeatherInterface
{
    protected function _construct()
    {
        $this->_init(
            ResourceModel::class
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * {@inheritDoc}
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * {@inheritDoc}
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * {@inheritDoc}
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * {@inheritDoc}
     */
    public function getTemperature()
    {
        return $this->getData(self::TEMPERATURE);
    }

    /**
     * {@inheritDoc}
     */
    public function setTemperature($temperature)
    {
        return $this->setData(self::TEMPERATURE, $temperature);
    }

    /**
     * {@inheritDoc}
     */
    public function getWeatherDescriptions()
    {
        return $this->getData(self::WEATHER_DESCRIPTIONS);
    }

    /**
     * {@inheritDoc}
     */
    public function setWeatherDescriptions($weather_descriptions)
    {
        return $this->setData(self::WEATHER_DESCRIPTIONS, $weather_descriptions);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {
        $createdAtObject = new \DateTime($createdAt);
        return $this->setData(self::CREATED_AT, $createdAtObject->format('Y-m-d H:i:s'));
    }
}
