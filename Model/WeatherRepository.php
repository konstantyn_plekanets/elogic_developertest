<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Model;

use Elogic\DeveloperTest\Api\Data\WeatherInterface;
use Elogic\DeveloperTest\Api\WeatherRepositoryInterface;
use Elogic\DeveloperTest\Model\ResourceModel\Collection\CollectionFactory as WeatherCollection;
use Elogic\DeveloperTest\Model\ResourceModel\ResourceModel;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class WeatherRepository
 * @package Elogic\DeveloperTest\Model
 */
class WeatherRepository implements WeatherRepositoryInterface
{
    /**
     * @var ResourceModel
     */
    private $weatherResource;

    /**
     * @var WeatherCollection
     */
    private $weatherCollection;

    /**
     * @var CollectionProcessor
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var WeatherFactory
     */
    private $weatherModelFactory;


    public function __construct(
        ResourceModel $weatherResource,
        WeatherCollection $weatherCollection,
        CollectionProcessor $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsFactory,
        WeatherFactory $weatherModelFactory
    ) {
        $this->weatherResource = $weatherResource;
        $this->weatherCollection = $weatherCollection;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->weatherModelFactory = $weatherModelFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function save(WeatherInterface $weather)
    {
        try {
            /** @var Weather|WeatherInterface $weather */
            $this->weatherResource->save($weather);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $weather;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(WeatherInterface $weather): bool
    {
        try {
            /** @var Weather $weather */
            $this->weatherResource->delete($weather);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete row. `%1` ', $e->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults
    {
        $collection = $this->weatherCollection->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritDoc}
     */
    public function getById($id): WeatherInterface
    {
        /** @var WeatherInterface $weather */
        $weather = $this->weatherModelFactory->create();
        $weather->load($id);
        if (!$weather->getId()) {
            throw new NoSuchEntityException(__('Weather (`%1`) does not exist with id = `%2`.', $weather, $id));
        }
        return $weather;
    }

    /**
     * {@inheritDoc}
     */
    public function deleteById($id)
    {
        try {
            $this->delete($this->getById($id));
            return __('Weather with id = `%1` was deleted successfully!', $id);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
