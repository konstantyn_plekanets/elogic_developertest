<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package Elogic\DeveloperTest\Model
 */
class Config
{
    /**
     * General settings
     *
     * @var string
     */
    const EXTENSION_ENABLED = "elogic_test/elogic_general_settings/enabled";
    const EXTENSION_CITY_NAME = "elogic_test/elogic_general_settings/city_name";
    const EXTENSION_API_REQUEST_URI = "elogic_test/elogic_general_settings/api_request_uri";
    const EXTENSION_ACCESS_KEY = "elogic_test/elogic_general_settings/access_key";

    /**
     * ScopeConfigInterface instance
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param null $scopeCode
     * @return bool
     */
    public function isEnabled($scopeCode = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::EXTENSION_ENABLED,
            ScopeInterface::SCOPE_WEBSITE,
            $scopeCode
        );
    }

    /**
     * @param null $scopeCode
     * @return string
     */
    public function getCityName($scopeCode = null): string
    {
        return $this->scopeConfig->getValue(
            self::EXTENSION_CITY_NAME,
            ScopeInterface::SCOPE_WEBSITE,
            $scopeCode
        );
    }

    /**
     * @param null $scopeCode
     * @return string
     */
    public function getApiRequestUri($scopeCode = null): string
    {
        return $this->scopeConfig->getValue(
            self::EXTENSION_API_REQUEST_URI,
            ScopeInterface::SCOPE_WEBSITE,
            $scopeCode
        );
    }

    /**
     * @param null $scopeCode
     * @return string
     */
    public function getAccessKey($scopeCode = null): string
    {
        return $this->scopeConfig->getValue(
            self::EXTENSION_ACCESS_KEY,
            ScopeInterface::SCOPE_WEBSITE,
            $scopeCode
        );
    }
}
