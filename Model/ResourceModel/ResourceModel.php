<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace  Elogic\DeveloperTest\Model\ResourceModel;

use Elogic\DeveloperTest\Model\Weather;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class ResourceModel
 * @package Elogic\DeveloperTest\Model\ResourceModel
 */
class ResourceModel extends AbstractDb
{
    CONST WEATHER_REQUESTS = 'elogic_weather_requests_entity';

    /**
     * ResourceModel constructor.
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(Context $context, $connectionName = null)
    {
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        // Table Name and Primary Key column
        $this->_init(
            self::WEATHER_REQUESTS,
            Weather::ENTITY_ID
        );
    }
}
