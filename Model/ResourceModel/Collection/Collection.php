<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Model\ResourceModel\Collection;

use Elogic\DeveloperTest\Model\ResourceModel\ResourceModel;
use Elogic\DeveloperTest\Model\Weather;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Elogic\DeveloperTest\Model\ResourceModel\Collection
 */
class Collection extends  AbstractCollection
{
    protected $idFieldName = Weather::ENTITY_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Weather::class,
            ResourceModel::class
        );
    }
}
