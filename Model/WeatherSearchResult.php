<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

namespace Elogic\DeveloperTest\Model;

use Elogic\DeveloperTest\Api\Data\WeatherSearchResultInterface as WeatherSearchResultInterfaceAlias;
use Magento\Framework\Api\SearchResults;

/**
 * Class WeatherSearchResult
 * @package Elogic\DeveloperTest\Model
 */
class WeatherSearchResult extends SearchResults implements WeatherSearchResultInterfaceAlias
{
    /**
     * {@inheritDoc}
     */
    public function getItems(): array
    {
        return parent::getItems();
    }
}
