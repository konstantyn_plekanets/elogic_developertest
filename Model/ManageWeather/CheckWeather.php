<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Model\ManageWeather;

use Elogic\DeveloperTest\Api\Data\WeatherInterface;
use Elogic\DeveloperTest\Model\Config;
use Elogic\DeveloperTest\Model\WeatherFactory;
use Elogic\DeveloperTest\Api\WeatherRepositoryInterface;
use Elogic\DeveloperTest\Service\WeatherServiceRequest;
use Psr\Log\LoggerInterface;

/**
 * Class CheckWeather
 * @package Elogic\DeveloperTest\Model
 */
class CheckWeather
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var WeatherServiceRequest
     */
    private $weatherServiceRequest;

    /**
     * @var WeatherRepositoryInterface
     */
    private $weatherRepository;

    /**
     * @var WeatherFactory
     */
    private $weather;

    /**
     * @var Config
     */
    private $weatherConfig;

    /**
     * CheckWeather constructor.
     * @param LoggerInterface $logger
     * @param WeatherServiceRequest $weatherServiceRequest
     * @param WeatherRepositoryInterface $weatherRepository
     * @param WeatherFactory $weather
     * @param Config $weatherConfig
     */
    public function __construct(
        LoggerInterface $logger,
        WeatherServiceRequest $weatherServiceRequest,
        WeatherRepositoryInterface $weatherRepository,
        WeatherFactory $weather,
        Config $weatherConfig
    ) {
        $this->logger = $logger;
        $this->weatherServiceRequest = $weatherServiceRequest;
        $this->weatherRepository = $weatherRepository;
        $this->weather = $weather;
        $this->weatherConfig = $weatherConfig;
    }

    /**
     * @return bool
     */
    public function checkWeather(): bool
    {
        if (!$this->weatherConfig->isEnabled()) return false;

        try {
            $weatherRequest = $this->weatherServiceRequest->doRequest();
            if (!$weatherRequest) {
                return false;
            }

            $country = $weatherRequest['location']['country'];
            $city = $weatherRequest['location']['name'];
            $temperature = $weatherRequest['current']['temperature'];
            $weatherDescriptions = $weatherRequest['current']['weather_descriptions'];
            !empty($weatherDescriptions)? $description = join(',', $weatherDescriptions) : $description = '';

            /** @var WeatherInterface $weather */
            $weather = $this->weather->create();
            $weather->setCountry($country);
            $weather->setCity($city);
            $weather->setTemperature($temperature);
            $weather->setWeatherDescriptions($description);

            $this->weatherRepository->save($weather);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return true;
    }
}
