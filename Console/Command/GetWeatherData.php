<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Console\Command;

use Elogic\DeveloperTest\Model\Config;
use Elogic\DeveloperTest\Model\ManageWeather\CheckWeather;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetWeatherData
 * @package Elogic\DeveloperTest\Console\Command
 */
class GetWeatherData extends  Command
{
    /**
     * @var CheckWeather
     */
    private $checkWeather;

    /**
     * @var Config
     */
    private $weatherConfig;

    /**
     * GetWeatherData constructor.
     * @param CheckWeather $checkWeather
     * @param Config $weatherConfig
     * @param string|null $name
     */
    public function __construct(
        CheckWeather $checkWeather,
        Config $weatherConfig,
        string $name = null
    ) {
        parent::__construct($name);
        $this->checkWeather = $checkWeather;
        $this->weatherConfig = $weatherConfig;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        if ($this->weatherConfig->isEnabled()) {
            $output->writeln("Started weather checking " .' '. date("Y-m-d H:i:s"));
            try {
                $this->checkWeather->checkWeather();
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
            }
            $output->writeln("Finished weather checking"  .' '. date("Y-m-d H:i:s"));
        } else {
            $output->writeln("Console command temporary disabled.");
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("elogic:weather_request");
        $this->setDescription("Get weather data from api");
        parent::configure();
    }
}
