<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Ui\DataProvider\Weather;

use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

/**
 * Class ListingDataProvider
 * @package Elogic\DeveloperTest\Ui\DataProvider\Weather
 */
class ListingDataProvider extends DataProvider
{

}