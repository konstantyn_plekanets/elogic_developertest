<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Service;

use Elogic\DeveloperTest\Model\Config;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class WeatherServiceRequest
 * @package Elogic\DeveloperTest\Service
 */
class WeatherServiceRequest
{
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var Config
     */
    private $config;

    /**
     * WeatherServiceRequest constructor.
     * @param RemoteAddress $remoteAddress
     * @param Json $serializer
     * @param ManagerInterface $messageManager
     * @param Curl $curl
     * @param Config $config
     */
    public function __construct(
        RemoteAddress $remoteAddress,
        Json $serializer,
        ManagerInterface $messageManager,
        Curl $curl,
        Config $config
    ) {
        $this->remoteAddress = $remoteAddress;
        $this->serializer = $serializer;
        $this->messageManager = $messageManager;
        $this->curl = $curl;
        $this->config = $config;
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function doRequest() : array
    {
        $responseData = null;
        try {
            $api_request_uri = $this->config->getApiRequestUri();
            $access_key = $this->config->getAccessKey();
            $query = $this->config->getCityName();
            $this->curl->get($api_request_uri . '?access_key=' . $access_key . '&query=' . $query);
            $response = $this->curl->getBody();
            $responseData = $this->serializer->unserialize($response);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e);
        }
        return $responseData;
    }
}
