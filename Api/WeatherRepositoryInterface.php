<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

namespace Elogic\DeveloperTest\Api;

use Elogic\DeveloperTest\Api\Data\WeatherInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface WeatherRepositoryInterface
 * @package Elogic\DeveloperTest\Api
 */
interface WeatherRepositoryInterface
{
    /**
     * @param WeatherInterface $weather
     *
     * @return WeatherInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(WeatherInterface $weather);

    /**
     * @param WeatherInterface $weather
     *
     * @return bool
     *
     * @throws LocalizedException
     */
    public function delete(WeatherInterface $weather);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResults
     *
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param $id
     *
     * @return WeatherInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById($id): WeatherInterface;

    /**
     * @param $id
     * @return void
     */
    public function deleteById($id);
}
