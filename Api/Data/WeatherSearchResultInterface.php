<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Api\Data;

/**
 * Interface WeatherSearchResultInterface
 * @package Elogic\DeveloperTest\Api\Data
 */
interface WeatherSearchResultInterface
{
    /**
     * @return WeatherInterface[]
     */
    public function getItems(): array;

    /**
     * @param WeatherInterface[] $items
     *
     * @return void
     */
    public function setItems(array $items);
}
