<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Api\Data;

/**
 * Interface WeatherInterface
 * @package Elogic\DeveloperTest\Api\Data
 */
interface WeatherInterface
{
    CONST ENTITY_ID = 'entity_id';

    CONST COUNTRY = 'country';

    CONST CITY = 'city';

    CONST TEMPERATURE = 'temperature';

    CONST WEATHER_DESCRIPTIONS = 'weather_descriptions';

    CONST CREATED_AT = 'created_at';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     *
     * @return void
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getCountry();

    /**
     * @param $country
     *
     * @return void
     */
    public function setCountry($country);

    /**
     * @return mixed
     */
    public function getCity();

    /**
     * @param $country
     *
     * @return void
     */
    public function setCity($country);

    /**
     * @return mixed
     */
    public function getTemperature();

    /**
     * @param $temperature
     *
     * @return void
     */
    public function setTemperature($temperature);

    /**
     * @return mixed
     */
    public function getWeatherDescriptions();

    /**
     * @param $weather_descriptions
     *
     * @return void
     */
    public function setWeatherDescriptions($weather_descriptions);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $createdAt
     *
     * @return void
     */
    public function setCreatedAt($createdAt);
}
