<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

namespace Elogic\DeveloperTest\Controller\Check;

use Elogic\DeveloperTest\Api\WeatherRepositoryInterface;
use \Elogic\DeveloperTest\Model\Weather as WeatherModel;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Weather
 * @package Elogic\DeveloperTest\Controller\Check
 */
class Weather implements ActionInterface
{
    /**
     * @var WeatherRepositoryInterface
     */
    private $weatherRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Weather constructor.
     * @param WeatherRepositoryInterface $weatherRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ResultFactory $resultFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        WeatherRepositoryInterface $weatherRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ResultFactory $resultFactory,
        LoggerInterface $logger
    ) {
        $this->weatherRepository = $weatherRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->resultFactory = $resultFactory;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        try {
            $weatherList = $this->weatherRepository->getList($searchCriteria)->getItems();

            if (empty($weatherList)) {
                $resultData = [];
            } else {
                /** @var WeatherModel $lastWeather */
                $lastWeather = end($weatherList);
                $resultData['city'] = $lastWeather->getCity();
                $resultData['temperature'] = $lastWeather->getTemperature();
                $resultData['time'] = $lastWeather->getCreatedAt();
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $resultData = [];
        }

        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($resultData);
        return $result;
    }
}
