<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

declare(strict_types=1);

namespace Elogic\DeveloperTest\Controller\Adminhtml\MassAction;

use Elogic\DeveloperTest\Model\ResourceModel\Collection\CollectionFactory;
use Elogic\DeveloperTest\Model\WeatherRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class MassDelete
 * @package Elogic\DeveloperTest\Controller\Adminhtml\MassAction
 */
class MassDelete extends Action
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var WeatherRepository
     */
    private $weatherRepository;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ResultFactory $resultFactory
     * @param WeatherRepository $weatherRepository
     * @param MessageManagerInterface $messageManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ResultFactory $resultFactory,
        WeatherRepository $weatherRepository,
        MessageManagerInterface $messageManager,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->filter = $filter;
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
        $this->context = $context;
        $this->weatherRepository = $weatherRepository;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try{
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $item) {
                $this->weatherRepository->deleteById($item->getId());
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 request(s) have been deleted.', $collectionSize));
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addErrorMessage(
                __('An error occurred while processing your delete  request. Please try again later.')
            );
        }
        return $resultRedirect->setPath('weather/index/');
    }
}
