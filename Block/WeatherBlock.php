<?php
/**
 * Elogic_DeveloperTest
 *
 * @copyright    Copyright (C) 2021 Elogic
 * @author       Konstantyn Plekanets <plekanets.konstantyn@elogic.com.ua>
 */

namespace Elogic\DeveloperTest\Block;

use Elogic\DeveloperTest\Model\Config;
use Elogic\DeveloperTest\Service\WeatherServiceRequest;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Psr\Log\LoggerInterface;

/**
 * Class WeatherBlock
 * @package Elogic\DeveloperTest\Block
 */
class WeatherBlock extends Template
{
    /**
     * @var Config
     */
    private $weatherConfig;

    /**
     * @var WeatherServiceRequest
     */
    private $weatherService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * WeatherBlock constructor.
     * @param Context $context
     * @param Config $weatherConfig
     * @param WeatherServiceRequest $weatherService
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $weatherConfig,
        WeatherServiceRequest $weatherService,
        LoggerInterface $logger,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->weatherConfig = $weatherConfig;
        $this->weatherService = $weatherService;
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function isWeatherApiEnabled(): bool
    {
        $result = false;
        try {
            $responseData = $this->weatherService->doRequest();
            if ($responseData &&
                !array_key_exists('success', $responseData) &&
                $this->weatherConfig->isEnabled()
            ) {
                $result = true;
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $result;
    }
}
